﻿using UnityEngine;
using System.Collections;

public class SpawnEpicenterOnClick : MonoBehaviour
{
    public GameObject prefab;

    void Clicked(Vector3 mouseLocation)
    {
        mouseLocation = new Vector3(mouseLocation.x, mouseLocation.y, -1);
        GameObject obj = Instantiate(prefab, mouseLocation, Quaternion.identity) as GameObject;
    }
}