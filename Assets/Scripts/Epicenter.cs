﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEditor;

public class Epicenter : MonoBehaviour
{
    public WavePoint WavePointPrefab;

    public List<WavePoint> WavePoints;
    public float TimeToLive = 5;
    public Material Material;


    private float StartTime;
    
    void Start()
    {
        StartTime = Time.time;

        CreateWavePoints();
    }

    private int NbWavePoints = 24;
    
    void CreateWavePoints()
    {
        for (int i = 1; i <= NbWavePoints; i++)
        {
            WavePoint wp = Instantiate(WavePointPrefab);
            wp.transform.position = transform.position;
            wp.transform.parent = gameObject.transform;
            wp.Direction = MathHelper.DegreeToVector2(i * 360 / NbWavePoints);
            Vector3 direction = new Vector3(wp.Direction.x, wp.Direction.y, 1);
            wp.transform.rotation = Quaternion.LookRotation(direction);
            WavePoints.Add(wp);
        }
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();

    }

    void Update()
    {
        if (StartTime + TimeToLive <= Time.time)
        {
            foreach (WavePoint wavePoint in WavePoints)
                wavePoint.StopMove();
        }

        drawMesh();
    }

    void drawMesh() { 
        MeshFilter mf = gameObject.GetComponent<MeshFilter>();

        Mesh mesh = new Mesh();
        mf.mesh = mesh;

        Renderer rend = gameObject.GetComponent<MeshRenderer>();
        rend.material = Material;

        Vector3[] vertices = new Vector3[WavePoints.Count + 1];
        vertices[0] = transform.position;
        for (int i = 0; i < WavePoints.Count; i++)
        {
            vertices[i + 1] = WavePoints[i].transform.position;
        }

        mesh.vertices = vertices;

        int[] triangles = new int[WavePoints.Count * 3];

        for (int i = 0; i < WavePoints.Count - 1; i++)
        {
            triangles[i * 3] = i + 2;
            triangles[i * 3 + 1] = 0;
            triangles[i * 3 + 2] = i + 1;
        }

        triangles[(WavePoints.Count - 1) * 3] = WavePoints.Count - 1;
        triangles[(WavePoints.Count - 1) * 3 + 1] = 0;
        triangles[(WavePoints.Count - 1) * 3 + 2] = 1;

        mesh.triangles = triangles;
        mesh.uv = BuildUVs(vertices);

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
    }

    public Vector3[] GetWavePoints()
    {
        Vector3[] result = new Vector3[WavePoints.Count];
        result[0] = Vector3.zero;
        for (int i = 0; i < WavePoints.Count; i++)
            result[i] = WavePoints[i].transform.position;
        return result;
    }

    //Vector2[] BuildUVs(Vector3[] vertices)
    //{

    //    float xMin = Mathf.Infinity;
    //    float yMin = Mathf.Infinity;
    //    float xMax = -Mathf.Infinity;
    //    float yMax = -Mathf.Infinity;

    //    foreach (Vector3 v3 in vertices)
    //    {
    //        if (v3.x < xMin)
    //            xMin = v3.x;
    //        if (v3.y < yMin)
    //            yMin = v3.y;
    //        if (v3.x > xMax)
    //            xMax = v3.x;
    //        if (v3.y > yMax)
    //            yMax = v3.y;
    //    }

    //    float xRange = xMax - xMin;
    //    float yRange = yMax - yMin;

    //    Vector2[] uvs = new Vector2[vertices.Length];
    //    for (int i = 0; i < vertices.Length; i++)
    //    {
    //        uvs[i].x = (vertices[i].x - xMin) / xRange;
    //        uvs[i].y = (vertices[i].y - yMin) / yRange;

    //    }
    //    return uvs;
    //}

    //Vector3 center;

    //int[] BuildTriangles(Vector3[] vertices)
    //{
    //    int[] result = new int[vertices.Length * 3];

    //    for (int i = 1; i < vertices.Length - 2; i++)
    //    {
    //        result[i * 3] = 0;
    //        result[i * 3 + 1] = i;
    //        result[i * 3 + 2] = i + 1;
    //    }

    //    return result;
    //}

    Vector2[] BuildUVs(Vector3[] vertices)
    {

        float xMin = Mathf.Infinity;
        float yMin = Mathf.Infinity;
        float xMax = -Mathf.Infinity;
        float yMax = -Mathf.Infinity;

        foreach (Vector3 v3 in vertices)
        {
            if (v3.x < xMin)
                xMin = v3.x;
            if (v3.y < yMin)
                yMin = v3.y;
            if (v3.x > xMax)
                xMax = v3.x;
            if (v3.y > yMax)
                yMax = v3.y;
        }

        float xRange = xMax - xMin;
        float yRange = yMax - yMin;

        Vector2[] uvs = new Vector2[vertices.Length];
        for (int i = 0; i < vertices.Length; i++)
        {
            uvs[i].x = (vertices[i].x + xMin) / xRange;
            uvs[i].y = (vertices[i].y - yMin) / yRange;

        }
        return uvs;
    }

}
