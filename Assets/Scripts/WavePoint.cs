﻿using UnityEngine;
using System.Collections;
using System;

public class WavePoint : MonoBehaviour
{

    public Epicenter ParentEpicenter;

    public Vector2 Direction;
    public float Speed = 0.1f;

    public WavePoint LeftWavePoint;
    public WavePoint RightWavePoint;

    void Start()
    {

    }
    
    void Update()
    {
        if (Direction == Vector2.zero) return;
        transform.Translate(Direction * Speed * Time.deltaTime);
    }

    internal void StopMove()
    {
        Direction = Vector2.zero;
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("test2");
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("test" + col.ToString());
    }
}
